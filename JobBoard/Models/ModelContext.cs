﻿using System;
using System.Data.Entity;
using System.Linq;

namespace JobBoard.Models
{
    public class ModelContext : DbContext
    {        
        public ModelContext()
            : base("ModelContext")
        {
        }
        public DbSet<Job> Jobs { get; set; }
    }       
}