﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JobBoard.Models
{
    public class Job
    {
        [Key]
        [Display(Name = "Job ID")]
        public int job { get; set; }
        
        [Display(Name = "Job Title")]
        [Required(ErrorMessage = "Job Title is required")]
        public string jobTitle { get; set; }

        [Display(Name = "Description")]        
        public string description { get; set; }

        [Display(Name = "Created At")]
        [Required(ErrorMessage = "Created At is required")]
        public DateTime createdAt { get; set; }

        [Display(Name = "Expires At")]
        [Required(ErrorMessage = "Expires At is required")]
        public DateTime expiresAt { get; set; }
    }
}